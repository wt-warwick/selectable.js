$.fn.selectable = function selectable() {
	var self = this;

	var colors = ['#e57373', '#4db6ac', '#ba68c8', '#90ca4d', '#7986cb', '#ffd54f', '#ff8a65'];
	var current;
	var markId = 0;
	var marks = [];
	var selecting = false;
	var text;

	function __construct() {
		text = normalize(self.text());
		addDOMCallbacks();
		refresh();
	}

	function addDOMCallbacks() {
		var state = 'idle';

		self.mousedown(function(event) {
			if (event.button === 0) {
				state = 'mousedown';
				self.trigger('mousedown');
			}
		});
		$(window).mousemove(function(event) {
			if (state === 'mousedown') {
				state = 'select'
				self.trigger('selectstart');
			}
		});
		$(window).mouseup(function(event) {
			var id;
			var range = [0, 0];
			var selection = getSelection();

			if (state === 'select') {
				range = getRange(selection);
			} else if (event.ctrlKey || event.metaKey) {
				range = sentenceRange(getOffset(selection));
			}

			if (range[1] - range[0] > 0) {
				id = self.mark(range[0], range[1]);
				self.trigger('select', [self.getElement(id), id]);
			}

			state = 'idle';
			selection.removeAllRanges();
		});
	}

	function previousRange(offset) {
		var i;
		var previous = null;

		for (i = 0; i < marks.length; i++) {
			if (marks[i].end < offset && (!previous || marks[i].end > previous[1])) {
				previous = [marks[i].start, marks[i].end];
			}
		}

		return previous;
	}

	function nextRange(offset) {
		var i;
		var next = null;

		for (i = 0; i < marks.length; i++) {
			if (marks[i].start > offset && (!next || marks[i].start < next[1])) {
				next = [marks[i].start, marks[i].end];
			}
		}

		return next;
	}

	function sentenceRange(offset) {
		var end;
		var i;
		var j;
		var next;
		var pre;
		var start;

		if (!offset || offset < 0) {
			i = j = 0;
		} else {
			i = j = offset;
			prev = previousRange(offset) || [0, 0];
			next = nextRange(offset) || [text.length, text.length];

			while (i > prev[1] && text.charAt(i) != '.')                            { i--; }
			while (i < next[0] && (text.charAt(i) == ' ' || text.charAt(i) == '.')) { i++; }
			while (j < next[0] && text.charAt(j) != '.')                            { j++; }
		}

		return [i, j];
	}

	self.clear = function clear() {
		marks = [];
		refresh();
	};

	self.getElement = function getElement(id) {
		return self.getElements().filter(function(index, element) {
			return $(element).data('id') === id;
		});
	};

	self.getElements = function getElements() {
		return self.find('.marked');
	};

	function getOffset(selection) {
		var contents;
		var i;
		var offset = 0;

		if (!selection                          || // No selection
			!selection.focusNode                || // No selection
		    selection.focusNode.nodeType !== 3) { // Selection doesn't start on a text node
			return -1;
		}

		contents = self.contents();

		for (i = 0; contents[i] != selection.focusNode; i++) {
			offset += $(contents[i]).text().length;
		}

		return offset + selection.focusOffset;
	}

	function getRange(selection) {
		var contents;
		var i;
		var next;
		var offset = 0;
		var prev;
		var range;

		if (!selection                                           || // No selection
			!selection.anchorNode                                || // No selection
			!selection.focusNode                                 || // No selection
		    selection.anchorNode !== selection.focusNode         || // Selection starts and ends in different text nodes (e.g. overlaps with a previous selection)
		    selection.anchorNode.nodeType !== 3                  || // Selection doesn't start on a text node
		    selection.focusOffset - selection.anchorOffset === 0) { // Selection of zero length
			return [0, 0];
		}

		contents = self.contents();

		for (i = 0; contents[i] != selection.anchorNode; i++) {
			offset += $(contents[i]).text().length;
		}

		range = [selection.anchorOffset + offset, selection.focusOffset + offset];
		range.sort(function(a, b) { return a - b; });

		prev = previousRange(range[0]) || [0, 0];
		next = nextRange(range[1]) || [text.length, text.length];

		while (range[0] >= prev[1] && isCharacter(text.charAt(range[0])))      { range[0]--; }
		while (range[0] >= prev[1] && !isCharacter(text.charAt(range[0])))     { range[0]++; }
		while (range[1] < next[0] && !isCharacter(text.charAt(range[1] - 1)))  { range[1]--; }
		while (range[1] < next[0] && isCharacter(text.charAt(range[1])))       { range[1]++; }

		return range;
	}

	function getSelection() {
		var selection = window.getSelection();
		return selection;
	}

	function html() {
		var color;
		var end;
		var i;
		var html = text;
		var offset = 0;
		var start;

		for (i = 0; i < marks.length; i++) {
			start = marks[i].start + offset;
			end   = marks[i].end + offset;
			color = marks[i].color;

			html = html.substring(0, start) + markHTML(marks[i].id, html.substring(start, end), color, marks[i].disabled) + html.substring(end);

			offset = html.length - text.length;
		}

		return html;
	}

	function isCharacter(char) {
		return char.length == 1 && char.match(/[a-zA-Z0-9]/);
	}

	self.isDisabled = function isDisabled(id) {
		var i;

		for (i = 0; i < marks.length; i++) {
			if (marks[i].disabled) {
				return true;
			}
		}

		return false;
	};

	function loadState() {
		self.getElements().each(function (index, element) {
			var i;

			for (i = 0; i < marks.length; i++) {
				if ($(element).data('id') === marks[i].id) {
					$(element).data(marks[i].data);
					return;
				}
			}
		});
	}

	self.mark = function mark(start, end, data, disabled) {
		var i;
		var mark;

		for (i = 0; i < marks.length; i++) {
			if (start >= marks[i].start && start < marks[i].end) {
				throw "Illegal Argument Exception: marks must not overlapping.";
			}
		}

		mark = {
			color:    nextColor(),
			data:     data,
			disabled: disabled ? true : false,
			end:      end,
			id:       markId++,
			start:    start
		};

		marks.push(mark);
		marks.sort(function(a, b) { return a.start - b.start; });

		refresh();

		$(self.getElement(mark.id)).data(data);

		return mark.id;
	};

	function markHTML(id, text, color, disabled) {
		var displayColor = disabled ? '#ccc' : color;
		return '<mark class="marked" data-color="' + color + '" data-id="' + id + '" style="background:' + displayColor + '">' + text + '</mark>';
	}

	function nextColor() {
		var i;
		var min = ['transparent', Number.MAX_VALUE];
		var prop;
		var usage = {};

		for (i = 0; i < colors.length; i++) {
			usage[colors[i]] = 0;
		}

		for (i = 0; i < marks.length; i++) {
			usage[marks[i].color]++;
		}

		for (prop in usage) {
			if (usage[prop] < min[1]) {
				min[0] = prop;
				min[1] = usage[prop];
			}
		}

		return min[0];
	}

	function normalize(text) {
		return text.replace(/\s\s+/g, ' ').trim();
	}

	function refresh() {
		saveState();
		self.empty();
		self.html(html());
		loadState();

		self.getElements().click(function(event) {
			var element = $(this);
			var i;
			var wasDisabled = false;

			for (i = 0; i < marks.length; i++) {
				if (element.data('id') === marks[i].id) {
					if (marks[i].disabled == true) {
						wasDisabled = true;
						marks[i].disabled = false;
					}
				}
			}

			self.trigger('mark-click', [$(this), $(this).data('id')]);

			if (wasDisabled) {
				refresh();
			}
		});
	}

	function saveState() {
		self.getElements().each(function (index, element) {
			var i;

			for (i = 0; i < marks.length; i++) {
				if ($(element).data('id') === marks[i].id) {
					marks[i].data = $(element).data();
					return;
				}
			}
		});
	}

	self.content = function content() {
		return text;
	}

	self.unmark = function unmark(id) {
		var fn;
		var dirty = false;

		if (id === undefined) {
			marks = [];
			dirty = true;
		} else {
			fn = typeof id === 'function' ? id : function(element) { return $(element).data('id') === id };

			self.getElements().each(function (index, element) {
				var i;

				if (fn(element)) {
					for (i = 0; i < marks.length; i++) {
						if (marks[i].id === $(element).data('id')) {
							marks.splice(i, 1);
							dirty = true;
							break;
						}
					}
				}
			});
		}

		if (dirty) {
			refresh();
		}
	};

	__construct();
	return this;
}
